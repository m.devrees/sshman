mod connectiontypes;
mod readhosts;

use crate::connectiontypes::ConnectionType;
use crate::readhosts::read_file;

fn main() {
    println!("Hello, world!");
    println!("1 + 2 = {}", 1u32 + 2);
    let _x = ConnectionType::SSH;
    let _ = read_file();
}
