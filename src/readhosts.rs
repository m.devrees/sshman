use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

pub fn read_file() -> std::io::Result<()> {
    let file = File::open("list.txt").unwrap();
    let reader = BufReader::new(file);
    // TODO should be line without prefixing underscore
    for _line in reader.lines() {
        // if line == "black=no" { ... } else { ... }
        // let parts = line.split("=");
        // optionally check whether parts.len() == 2
        // let color = parts[0];
        // let value = parts[1];
// do stuff
    }
    Ok(())
}
